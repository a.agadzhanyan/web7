window.addEventListener('DOMContentLoaded', function() {

	'use strict';
	let slideIndex = 1,
		slides = document.querySelectorAll('.slider-item'),
		prev = document.querySelector('.prev'),
		next = document.querySelector('.next'),
		dotsWrap = document.querySelector('.slider-dots'),
		dots = document.querySelectorAll('.dot');

	showSlides(slideIndex);

	//функция, которая будет показывать определенный слайд
	function showSlides(n) {

		if (n > slides.length) {
			slideIndex = 1;
		}
		if (n < 1) {
			slideIndex = slides.length;
		}

		slides.forEach((item) => item.style.display = 'none');//все слайды скроются
		dots.forEach((item) => item.classList.remove('dot-active'));//все точки не активны

		slides[slideIndex - 1].style.display = 'block';
		dots[slideIndex - 1].classList.add('dot-active');
	}

	//функция, которая изменяет текущий слайд
	function plusSlides(n) {
		showSlides(slideIndex += n);
	}

	//функция, которая будет определять текущий слайд
	function currentSlides(n) {
		showSlides(slideIndex = n);
	}

	//реализация стрелки назад
	prev.addEventListener('click', function() {
		plusSlides(-1);
	});

	//реализация стрелки вперед
	next.addEventListener('click', function() {
		plusSlides(1);
	});

	//реализация перемещения при клике на точку
	dotsWrap.addEventListener('click', function (event) {
		for (let i = 0; i < dots.length + 1; ++i) {
			if (event.target.classList.contains('dot') && event.target == dots[i-1]) {
				currentSlides(i);
			}
		}
	});
});
